export interface Driver {
    firstName: string;
    lastName: string;
    truckID: string;
    trailerID: string;
}