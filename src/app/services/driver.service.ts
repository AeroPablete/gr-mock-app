import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Driver } from '../models/driver.model';

@Injectable({
    providedIn: 'root'
})
export class DriverService {

    constructor(private httpClient: HttpClient) { }

    /**
     * Fetches drivers data
     */
    getDriverData(): Observable<Driver[]> {
        return this.httpClient.get<Driver[]>("assets/data/drivers.json");
    }
}
