import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriverListComponent } from './driver-list/driver-list.component';
import { DriverItemComponent } from './driver-item/driver-item.component';
import { DriverFilterComponent } from './driver-filter/driver-filter.component';
import { DriverWrapperComponent } from './driver-wrapper/driver-wrapper.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
    declarations: [
        DriverListComponent,
        DriverItemComponent,
        DriverFilterComponent,
        DriverWrapperComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        SharedModule
    ],
    exports: [
        DriverWrapperComponent
    ]
})
export class DriversModule { }
