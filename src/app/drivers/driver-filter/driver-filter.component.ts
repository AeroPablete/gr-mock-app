import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

export interface DriverFilterValue {
    driverName: string
}

@Component({
    selector: 'app-driver-filter',
    templateUrl: './driver-filter.component.html',
    styleUrls: ['./driver-filter.component.css']
})
export class DriverFilterComponent implements OnInit {

    /** Form */
    form = new FormGroup({
        driverName: new FormControl(''),
    });

    /** Form Submission Event Emitter */
    @Output('search')
    formSubmissionEventEmitter: EventEmitter<DriverFilterValue> = new EventEmitter();

    /** Form Reset Event Emitter */
    @Output('reset')
    formResetEventEmitter: EventEmitter<void> = new EventEmitter();

    /**
     * 
     */
    constructor() {}

    /**
     * 
     */
    ngOnInit(): void {}

    /**
     * 
     */
    onResetForm() {
        this.form.reset();
        this.formResetEventEmitter.emit();
    }

    /**
     * 
     */
    onSubmitForm() {
        this.formSubmissionEventEmitter.emit(this.form.value);
    }
}
