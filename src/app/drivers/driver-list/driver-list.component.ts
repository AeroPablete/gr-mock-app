import { Component, Input, OnInit } from '@angular/core';
import { Driver } from 'src/app/models/driver.model';

@Component({
    selector: 'app-driver-list',
    templateUrl: './driver-list.component.html',
    styleUrls: ['./driver-list.component.css']
})
export class DriverListComponent implements OnInit {

    /** Driver List */
    @Input()
    driverList: Driver[] = [];

    /**
     * 
     */
    constructor() { }

    /**
     * 
     */
    ngOnInit(): void {
    }

}
