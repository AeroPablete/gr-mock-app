import { Component, OnInit } from '@angular/core';
import { Driver } from 'src/app/models/driver.model';
import { DriverService } from 'src/app/services/driver.service';
import { DriverFilterValue } from '../driver-filter/driver-filter.component';

@Component({
    selector: 'app-driver-wrapper',
    templateUrl: './driver-wrapper.component.html',
    styleUrls: ['./driver-wrapper.component.css']
})
export class DriverWrapperComponent implements OnInit {

    /** Driver List */
    driverList: Driver[] = [];

    /** Filtered Driver List */
    filteredDriverList: Driver[] = [];

    /**
     * 
     * @param driverService 
     */
    constructor(private driverService: DriverService) { }

    /**
     * 
     */
    ngOnInit(): void {
        this._getDriverData().subscribe(data => {
            this.driverList = data;
            this.filteredDriverList = data;
        });
    }

    /**
     * 
     * @param event 
     */
    onSearch(query: DriverFilterValue) {
        const searchValue = query.driverName
            .normalize("NFD").replace(/[\u0300-\u036f]/g, "")
            .toLowerCase();

        const results = this.driverList.filter(d => {
            const fullName = `${d.firstName} + ${d.lastName}`
                .normalize("NFD").replace(/[\u0300-\u036f]/g, "")
                .toLowerCase();
            
            return fullName.includes(searchValue);
        })
        
        this.filteredDriverList = results;
    }

    /**
     * 
     */
    onReset() {
        this.filteredDriverList = this.driverList;
    }

    /**
     * 
     */
    private _getDriverData() {
        return this.driverService.getDriverData();
    }
}
