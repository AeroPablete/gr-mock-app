import { Component, Input, OnInit } from '@angular/core';
import { Driver } from 'src/app/models/driver.model';

@Component({
    selector: 'app-driver-item',
    templateUrl: './driver-item.component.html',
    styleUrls: ['./driver-item.component.css']
})
export class DriverItemComponent implements OnInit {

    /** Driver */
    @Input()
    driver!: Driver;

    constructor() { }

    ngOnInit(): void {
    }

}
