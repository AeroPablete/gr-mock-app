import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-icon',
    templateUrl: './icon.component.html',
    styleUrls: ['./icon.component.css']
})
export class IconComponent implements OnInit {

    /** Icon Name */
    @Input()
    iconName = "";

    /** Icon Size */
    @Input()
    iconSize = ""

    /** Icon Tooltip */
    @Input()
    iconTooltip = "This is a tooltip"

    constructor() { }

    ngOnInit(): void {
    }

}
