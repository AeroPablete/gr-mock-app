import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-accordion',
    templateUrl: './accordion.component.html',
    styleUrls: ['./accordion.component.css']
})
export class AccordionComponent implements OnInit {

    /** Accordion status */
    isOpen = false;

    /** Accordion heading */
    @Input('heading') heading = "";

    constructor() { }

    ngOnInit(): void {
    }

    /**
     * 
     */
    toggleAccordion() {
        this.isOpen = !this.isOpen;
    }
}
