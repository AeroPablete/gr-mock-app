import { Component, ContentChildren, Input, OnInit, QueryList } from '@angular/core';
import { TabItemComponent } from './tab-item/tab-item.component';

@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

    /** Labels */
    @Input() labels : string[] = [];
    
    /** Children */
    @ContentChildren(TabItemComponent) children : QueryList<TabItemComponent> = new QueryList();

    /** Active Tab Index */
    activeTabIndex = 0;

    /**
     * 
     */
    constructor() { }    

    /**
     * 
     */
    ngOnInit(): void {
    }

    /**
     * 
     * @param index 
     */
    onTabClick(index: number) {
        this.activeTabIndex = index;
    }

}
