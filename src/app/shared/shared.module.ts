import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SummaryComponent } from './summary/summary.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabItemComponent } from './tabs/tab-item/tab-item.component';
import { AccordionComponent } from './accordion/accordion.component';
import { IconComponent } from './icon/icon.component';

@NgModule({
    declarations: [
        AccordionComponent,
        HeaderComponent,
        IconComponent, 
        SummaryComponent,
        TabsComponent,
        TabItemComponent,       
    ],
    imports: [
        CommonModule
    ],
    exports: [
        AccordionComponent,
        HeaderComponent,
        IconComponent,
        SummaryComponent,
        TabsComponent,
        TabItemComponent
    ]
})
export class SharedModule { }
