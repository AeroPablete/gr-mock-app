import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderFilterComponent } from './order-filter/order-filter.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderItemComponent } from './order-item/order-item.component';
import { OrderWrapperComponent } from './order-wrapper/order-wrapper.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        OrderFilterComponent,
        OrderListComponent,
        OrderItemComponent,
        OrderWrapperComponent
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        OrderWrapperComponent
    ]
})
export class OrdersModule { }
